package nginx

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

//Site contains the information for an Nginx configuration site
type Site struct {
	Name    string `json:"site_name"` //name of the site file
	IsNew   bool   `json:"-"`
	Enabled bool   `json:"enabled"`
	Server  Server `json:"server"`
}

// NewSite creates a new configuration object with the SiteName specified
func NewSite(siteName string) *Site {
	return &Site{Name: siteName}
}

//NewSiteFromJSON is used to parse a site that is in a json-encoded []byte
func NewSiteFromJSON(payload []byte) (*Site, error) {
	site := new(Site)
	err := json.Unmarshal(payload, site)
	return site, err
}

// Save writes the configuration to the JSON file for storage. The JSON file is
// stored in the configuration folder with the name <SiteName>.json
func (s *Site) Save(directory string) error {
	content, err := json.Marshal(s)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(directory+s.Name+".json", content, 0744)
	f, err := os.OpenFile(directory+s.Name, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0744)
	if err != nil {
		return err
	}
	s.writeConfig(f)
	return err
}

// Load reads the configuration from the JSON file.
func (s *Site) Load(directory string) error {
	content, err := ioutil.ReadFile(directory + s.Name + ".json")
	if err != nil {
		return err
	}
	err = json.Unmarshal(content, s)
	return err
}

func (s *Site) writeConfig(w io.Writer) {
	fmt.Fprintf(w, "# Server Name: %s\n", s.Name)
	s.Server.writeConfig(w)
}
