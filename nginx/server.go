package nginx

import (
	"fmt"
	"io"
)

// Server contains the information for a server
type Server struct {
	Listens     []Listen    `json:"listen"`
	ServerNames ServerNames `json:"server_names"`
	Locations   []Location  `json:"locations"`
}

func (s *Server) writeConfig(w io.Writer) {
	fmt.Fprintf(w, "server {\n")
	for _, listen := range s.Listens {
		listen.writeConfig(w)
	}
	s.ServerNames.writeConfig(w)
	for _, location := range s.Locations {
		location.writeConfig(w)
	}
	fmt.Fprintf(w, "}\n")
}
