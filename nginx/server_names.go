package nginx

import (
	"fmt"
	"io"
)

// ServerNames contains an array of names used by the virtual server
type ServerNames []string

func (s ServerNames) writeConfig(w io.Writer) {
	fmt.Fprintf(w, `server_name`)
	for _, serverName := range s {
		fmt.Fprintf(w, ` %s`, serverName)
	}
	fmt.Fprintf(w, ";\n")
}
