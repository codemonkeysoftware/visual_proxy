package nginx

import (
	"fmt"
	"io"
)

// Listen contains the configuration for listen addresses
type Listen struct {
	Address       string `json:"address"`
	DefaultServer bool   `json:"default"`
}

const listenBlock = `listen %s%s;
`

func (l *Listen) writeConfig(w io.Writer) {
	defaultServer := ""
	if l.DefaultServer {
		defaultServer = " default_server"
	}
	fmt.Fprintf(w, listenBlock, l.Address, defaultServer)
}
