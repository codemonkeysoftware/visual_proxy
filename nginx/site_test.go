package nginx

import (
	"bytes"
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/stretchr/testify/assert"
)

const testConfigPath = `../testdata/sites/`

func TestReadWriteSite(t *testing.T) {
	name := "06-blank-config"
	//create sample configuration
	site := NewSite(name)
	err := site.Save(testConfigPath)
	assert.Nil(t, err)

	site2 := NewSite(name)
	err = site.Load(testConfigPath)
	assert.Nil(t, err)

	assert.Equal(t, site, site2)

	//check that the config file gets written
	b, err := ioutil.ReadFile(testConfigPath + name)
	require.Nil(t, err)
	assert.NotEmpty(t, b)

}

func TestWriteConfig(t *testing.T) {
	expected := `# Server Name: test_server
server {
listen 80 default_server;
listen [::]:80;
server_name test_server.example.com test_server2.example.com;
location / {
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $remote_addr;
proxy_set_header Host $host;
proxy_pass http://127.0.0.1:8080;
}
}
`
	site := &Site{
		Name: `test_server`,
		Server: Server{
			Listens: []Listen{{
				Address:       `80`,
				DefaultServer: true,
			}, {
				Address:       `[::]:80`,
				DefaultServer: false,
			}},
			ServerNames: ServerNames{`test_server.example.com`, `test_server2.example.com`},
			Locations: []Location{{
				Path:          `/`,
				BackendServer: `http://127.0.0.1:8080`,
			}},
		},
	}

	w := bytes.NewBuffer(nil)
	site.writeConfig(w)
	assert.Equal(t, expected, w.String())
}
