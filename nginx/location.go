package nginx

import (
	"fmt"
	"io"
)

// Location contains the configuration for one location
type Location struct {
	Path          string `json:"path"`
	BackendServer string `json:"backend_server"`
}

const locationBlock = `location %s {
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $remote_addr;
proxy_set_header Host $host;
proxy_pass %s;
}
`

func (l *Location) writeConfig(w io.Writer) {
	fmt.Fprintf(w, locationBlock, l.Path, l.BackendServer)
}
