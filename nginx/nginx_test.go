package nginx

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestGetNginxSiteNames(t *testing.T) {
	siteNames, err := GetNginxSitesAvailable(nginxSitesDir)
	require.Nil(t, err)
	_, ok := siteNames["01-default"]
	assert.True(t, ok)
}
