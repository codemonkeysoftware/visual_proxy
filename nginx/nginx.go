package nginx

import (
	"io/ioutil"
)

const nginxSitesDir = "../testdata/sites/"

// GetNginxSitesAvailable gets a map of the sites available
func GetNginxSitesAvailable(siteDir string) (map[string][]byte, error) {
	files, err := ioutil.ReadDir(siteDir)
	if err != nil {
		return nil, err
	}
	siteNames := make(map[string][]byte, len(files))
	for _, file := range files {
		if file.IsDir() {
			continue
		}
		siteNames[file.Name()] = nil
	}
	return siteNames, nil
}
